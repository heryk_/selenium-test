from selenium.webdriver.common.by import By
import os

class CalcPage(object):
	"""A class for main page locators. All main page locators should come here"""
	def __init__(self, driver):
		self.driver = driver
		self.open()
		self.result_box = self.driver.find_element(By.ID, 'resultsbox') 
		self.equal = self.driver.find_element(By.NAME, '=')
		self.clean = self.driver.find_element(By.NAME, 'C')

	def clear(self):
		self.clean.click()

	def calculate(self, expression):
		for char in expression:
			self.driver.find_element(By.NAME,char).click()
		self.equal.click();
		return self.result_box.get_attribute("value")

	def open(self):
		self.driver.get("file:///{}/{}".format(os.getcwd(),'calc.html'))
    