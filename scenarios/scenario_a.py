# -*- coding: utf8 -*- 
import unittest
from webdriver_manager.chrome import ChromeDriverManager 
from selenium import webdriver
from ddt import ddt, data, unpack
 
from lib.get_data import get_csv_data
from lib.page import CalcPage
 
 
@ddt
class TestScenarioA(unittest.TestCase):
    """ inheriting the TestCase class"""
 
    @classmethod
    def setUpClass(cls):
        """test preparation"""
        cls.driver = webdriver.Chrome(ChromeDriverManager().install())

        cls.driver.implicitly_wait(3)
        
        cls.calc = CalcPage(cls.driver) 
 
    @data(*get_csv_data('./data/scenario_a.csv'))
    @unpack
    def test_calculation(self, statemant, exp_res):
        """test case for scenario a"""
        
        self.calc.clear()
        assert self.calc.calculate(statemant) == exp_res
 
    @classmethod
    def tearDownClass(cls):
        """clean up"""
        cls.driver.close()